import java.io.*;
import java.util.*;

public class Lab9
{
	public static void main(String args[]) throws Exception
	{
		BufferedReader infile = new BufferedReader(new FileReader(args[0]));
		HashMap<String,Integer> histogram = new HashMap<String,Integer>();
		String word;
		while ( infile.ready() )
		{
			word = infile.readLine();
			
			if (!histogram.containsKey(word))
				histogram.put(word, 1);
			else
				histogram.put(word, histogram.get(word) + 1);
				 // YOUR CODE HERE
		}
		infile.close();
		printHistogram( histogram );
	} // END MAIN

	// YOU FILL IN THIS METHOD
	// NO PARTIAL CREDIT. YOU MUST PRINT THEM SORTED LIKE THE OUPTUT
	private static void printHistogram( HashMap<String,Integer> hm )
	{
		ArrayList<String> keyList = new ArrayList<String>();
		
		for (String token: hm.keySet())
			keyList.add(token);
		
		Collections.sort(keyList);
		
		for (String token: keyList)
			System.out.println(token + "	" + hm.get(token));
		// YOUR CODE HERE
		// Initialize an ArrayList by feeding the keyset of the map into the ArrayList constructor
		// sort the ArrayList
		// for each key in the ArrayList println the key followed by a TAB followed by the freq.  (use hm.get(key))
	}
} // END LAB9 CLASS